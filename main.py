import argparse
import json
import sys
import requests


def api_request(method: str, token: str, params: dict):
    url = f'https://api.vk.com/method/{ method }?v=5.131&access_token={ token }'
    if params:
        url += '&' + \
            '&'.join(map(lambda key_value_pair: '='.join(
                map(str, key_value_pair)), params.items()))

    return requests.get(url).text


def handle_res(result: str):
    res_dict = json.loads(result)
    if not(res_dict['response']):
        return (True, [])

    return (False, res_dict['response'])


def handled_api_request(method: str, token: str, params: dict):
    return handle_res(api_request(method, token, params))


def exit_with_msg(*args):
    print(*args)
    exit(0)


def init(token: str, target: str):
    def basic_user_data(user_ids_array):
        is_empty, result = handled_api_request(
            'users.get', token, dict(user_ids=json.dumps(user_ids_array)))
        return result

    def nickname_to_user_id(nickname: str):
        is_empty, result = handled_api_request(
            'users.get', token, dict(user_ids=nickname))
        if is_empty:
            exit_with_msg('Пользователь не найден:', nickname)
        return result[0]['id']

    def friends_data(user_id: str):
        is_empty, result = handled_api_request(
            'friends.get', token, dict(user_id=user_id))
        return result

    user_id = nickname_to_user_id(target)
    friends_data = friends_data(user_id)
    print('Число друзей:', friends_data['count'], '\n')
    friends_extended_data = basic_user_data(friends_data['items'])

    deactivated_count = len(
        [*filter(lambda friend_data: 'deactivated' in friend_data, friends_extended_data)])
    for (ix, friend_data) in enumerate(filter(lambda friend_data: 'deactivated' not in friend_data, friends_extended_data)):
        print(f'{ix + 1}.' + ('🔒' if friend_data['is_closed'] else ''),
              f'{friend_data["first_name"]} {friend_data["last_name"]}')

    print('\nИ ещё', deactivated_count, 'удалённых аккаунтов.')


parser = argparse.ArgumentParser(description="Shows info on someone's vk friends.")
parser.add_argument("target", help="vk id or nickname")
parser.add_argument("token", help="access token used for requests")
params = parser.parse_args()

init(params.token, params.target)
