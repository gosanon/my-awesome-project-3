# Task 3
## Help message
```python
python main.py -h
```
OR
```python
python main.py --help
```
## Example usage
```
python main.py gosanon toooooooookeeeeeeeeeeeeeeeeeeennnnnn
```
*(id instead of nickname is also supported)*
